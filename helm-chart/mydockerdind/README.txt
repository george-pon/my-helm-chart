# mydockerdind

docker:dindイメージをhelm chartからdeploymentとして起動する。kubernetes serviceも起動する。

docker client コマンドをインストール済みの他のPodから環境変数を指定すると、docker buildできるようになる。

/var/run/docker.sockは使用していない単独動作も可能。

### 使用例

```
# 環境変数 DOCKER_HOST に通信先を指定。暗号化や認証は設定してないので注意。
DOCKER_HOST=tcp://mydockerdind:2375
docker version

# 試しにビルドしてみる
cat > Dockerfile << "EOF"
FROM centos:centos7
EOF
docker build .
```


### ネットからのインストール

```
#
# mydockerdind
#
function f-helm-mydockerdind() {
if true; then
    helm repo add myhelmchartrepo  https://gitlab.com/george-pon/my-helm-chart/raw/master/helm-chart/charts/
    helm repo update
    helm search mydockerdind
    helm inspect myhelmchartrepo/mydockerdind
    helm delete --purge mydockerdind
    sleep 5
    helm install myhelmchartrepo/mydockerdind \
        --name mydockerdind
    kubectl rollout status deploy/mydockerdind
fi
}
```


### ローカルでのテスト手順

```
#
#  インストール
#
cd /c/home/git/my-helm-chart/helm-chart
helm template mydockerdind --name mydockerdind
helm install  mydockerdind --name mydockerdind

#
#  アップグレード
#
cd /c/home/git/my-helm-chart/helm-chart
helm upgrade  mydockerdind  mydockerdind

#
#  調査
#
kube-all.sh get pod,pv,pvc
POD_NAME=$( kubectl get pod | grep mydockerdind | awk '{print $1}'  )
kubectl describe pod $POD_NAME
kubectl logs $POD_NAME

#
#  アンインストール
#
helm delete --purge mydockerdind
```


