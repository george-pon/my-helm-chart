# mymongodb

mongoDB レプリカセットの実験

statefulsetでmongoDBを3個建てる。

* 元ネタ https://kubernetes.io/blog/2017/01/running-mongodb-on-kubernetes-with-statefulsets/ Running MongoDB on Kubernetes with StatefulSets
* https://hub.docker.com/r/cvallance/mongo-k8s-sidecar
* https://github.com/cvallance/mongo-k8s-sidecar

### 使い方

mongoDBのアクセス先の例 
1. mymongodb-0.mymongodb.default.svc.cluster.local
2. mymongodb-1.mymongodb.default.svc.cluster.local
3. mymongodb-2.mymongodb.default.svc.cluster.local

### ネットからのインストール

```
#
# mymongodb
#
function f-helm-mymongodb() {
if true; then
    helm repo add myhelmchartrepo  https://gitlab.com/george-pon/my-helm-chart/raw/master/helm-chart/charts/
    helm repo update
    helm search mymongodb
    helm inspect myhelmchartrepo/mymongodb
    helm delete --purge mymongodb
    sleep 5
    helm install myhelmchartrepo/mymongodb \
        --name mymongodb \
        --set ingress.hosts="{mymongodb.minikube.local}"
    kubectl rollout status statefulset/mymongodb
fi
}
```

### ローカルでのテスト手順

```
#
# chartチェック
#
cd /c/home/git/my-helm-chart/helm-chart
helm template mymongodb --name mymongodb

#
# インストール
#
cd /c/home/git/my-helm-chart/helm-chart
chart=mymongodb
PRESENT=$( helm list $chart )
if [ -z "$PRESENT" ] ; then
    # use local image name
    helm install $chart --name $chart
else
    # use local image name
    helm upgrade $chart $chart
fi

#
#  調査
#
kube-all.sh get pod,pv,pvc
kubectl describe  statefulset mymongodb
kubectl describe pod mymongodb-0

cd /c/home/git/my-helm-chart/helm-chart
helm install mymongodb --name mymongodb-a
helm install mymongodb --name mymongodb-b
helm install mymongodb --name mymongodb-c
kube-all.sh get pod,pv,pvc

#
# 調査用
#
kube-all.sh get pod,svc,pv,pvc

stern  mongo

#
# ログを見てメンバーのPRIMARY, SECONDARY, RS_DOWNなどの様子を見る
#
kubectl logs  mymongodb-0 -c mymongodb | grep Member
kubectl logs  mymongodb-1 -c mymongodb | grep Member
kubectl logs  mymongodb-2 -c mymongodb | grep Member

#
# mymongodb-2のPVCマウントの様子がおかしい？ 
#
kube-all.sh get pod,svc,pv,pvc
kubectl describe pod mymongodb-2
kubectl delete persistentvolumeclaim/datadir-mymongodb-2
kubectl delete pod mymongodb-2


mymongodb-1.mymongodb.default.svc.cluster.local:27017

statefulset.kubernetes.io/pod-name=mymongodb-0


#
# 削除
#
helm delete  --purge  mymongodb
helm delete  --purge  mymongodb-a
helm delete  --purge  mymongodb-b
helm delete  --purge  mymongodb-c
kubectl delete persistentvolumeclaim/datadir-mymongodb-0
kubectl delete persistentvolumeclaim/datadir-mymongodb-1
kubectl delete persistentvolumeclaim/datadir-mymongodb-2
kubectl delete persistentvolumeclaim/datadir-mymongodb-a-0
kubectl delete persistentvolumeclaim/datadir-mymongodb-a-1
kubectl delete persistentvolumeclaim/datadir-mymongodb-a-2
kubectl delete persistentvolumeclaim/datadir-mymongodb-b-0
kubectl delete persistentvolumeclaim/datadir-mymongodb-b-1
kubectl delete persistentvolumeclaim/datadir-mymongodb-b-2
kubectl delete persistentvolumeclaim/datadir-mymongodb-c-0
kubectl delete persistentvolumeclaim/datadir-mymongodb-c-1
kubectl delete persistentvolumeclaim/datadir-mymongodb-c-2
kube-all.sh get pod,pv,pvc
```

