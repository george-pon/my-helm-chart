# my-helm-chart

オレの helm chart

## mydockerdind

docker:dindイメージをhelm chartからdeploymentとして起動する。kubernetes serviceも起動する。

docker client コマンドをインストール済みの他のPodから環境変数を指定すると、docker buildできる。

dockerホスト機の/var/run/docker.sockは使用していない単独動作。

### 使用例

```
# 環境変数 DOCKER_HOST に通信先を指定。暗号化や認証は設定してないので注意。
DOCKER_HOST=tcp://mydockerdind:2375
docker version

# 試しにビルドしてみる
cat > Dockerfile << "EOF"
FROM centos:centos7
EOF
docker build .
```


### mydockerdind ネットからのインストール

```
#
# mydockerdind
#
function f-helm-mydockerdind() {
if true; then
    helm repo add myhelmchartrepo  https://gitlab.com/george-pon/my-helm-chart/raw/master/helm-chart/charts/
    helm repo update
    helm search mydockerdind
    helm inspect myhelmchartrepo/mydockerdind
    helm delete --purge mydockerdind
    sleep 5
    helm install myhelmchartrepo/mydockerdind \
        --name mydockerdind
    kubectl rollout status deploy/mydockerdind
fi
}
```

### mymongodb

mongoDB replicaSet 3 の実験

### myhttpd2

apache httpd 2.4系のサンプル


