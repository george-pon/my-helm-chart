#!/bin/bash
#
# build helm chart package
#

chart=$1
if [ -z "$chart" ]; then
    echo "package-helm-chart.sh  chart-directory-name"
    exit 1
fi

set -ex

mkdir -p ./helm-chart/charts
pushd ./helm-chart/charts
helm package ../$chart
helm repo index  --url=https://gitlab.com/george-pon/my-helm-chart/raw/master/helm-chart/charts/  .
popd

set +ex
