#!/bin/bash

function f-release-preview() {

    DO_EDIT=no
    if [ x"$1"x = x"--edit"x ]; then
        DO_EDIT=yes
    fi

    EDITOR=${EDITOR:-vi}
    echo EDITOR is $EDITOR

    set -eux
    kubectl version
    helm version
    set +eux

    for chart in mymongodb mydockerdind myhttpd2
    do

        if [ x"$DO_EDIT"x = x"yes"x ]; then
            # edit version number
            $EDITOR README.md CHANGELOG.md helm-chart/$chart/Chart.yaml helm-chart/$chart/templates/NOTES.txt
        fi

        # build helm-chart package
        bash package-helm-chart.sh $chart

        # test run via helm
        pushd helm-chart
            # check if kjwikigdocker is present.
            PRESENT=$( helm list $chart )
            if [ -z "$PRESENT" ] ; then
                # use local image name
                helm install $chart --name $chart
            else
                # use local image name
                helm upgrade $chart $chart
            fi
        popd

    done

    echo ""
    echo "Note"
    echo ""

}

f-release-preview "$@"

